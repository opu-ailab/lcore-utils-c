#include <stdlib.h>
#include <check.h>
#include "../src/lcore_utils.h"

void
setup (void)
{
    /* none */
}

void
teardown (void)
{
    unsetenv("LCORE_LOG_LEVEL");
}

START_TEST (test_lcore_system_exec_command)
{
    ck_assert_int_eq (lcore_system ("/bin/echo 1"), 0);
}
END_TEST

START_TEST (test_lcore_system_display_and_exec_command)
{
    setenv("LCORE_LOG_LEVEL", "info", 1);
    ck_assert_int_eq (lcore_system ("/bin/echo 1"), 0);
}
END_TEST

START_TEST (test_lcore_fopen_display_and_open_file)
{
    setenv("LCORE_LOG_LEVEL", "info", 1);
}
END_TEST

int main(void)
{
    return 0;
}
