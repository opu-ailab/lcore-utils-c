#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>

#ifndef LCORE_UTILS_H
#define LCORE_UTILS_H

int lcore_system(const char *command);
FILE *lcore_fopen(const char *path, const char *mode);
int lcore_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
ssize_t lcore_send(int sockfd, const void *buf, size_t len, int flags);
ssize_t lcore_recv(int sockfd, void *buf, size_t len, int flags);
void lcore_info(const char *str);

#endif /* LCORE_UTILS_H */
