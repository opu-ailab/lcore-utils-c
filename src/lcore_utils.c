#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "lcore_utils.h"

int lcore_system(const char *command)
{
    char *level = getenv("LCORE_LOG_LEVEL");
    if (level != NULL && strcmp(level, "info") == 0) {
        fprintf(stderr, "EXEC: %s\n", command);
    }
    return system(command);
}

FILE *lcore_fopen(const char *path, const char *mode)
{
    char *level = getenv("LCORE_LOG_LEVEL");
    if (level != NULL && strcmp(level, "info") == 0) {
        fprintf(stderr, "OPEN FILE: %s (mode: %s)\n", path, mode);
    }
    return fopen(path, mode);
}

int lcore_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    char *level = getenv("LCORE_LOG_LEVEL");
    if (level != NULL && strcmp(level, "info") == 0) {
        fprintf(stderr, "CONNECT: \n");
    }
    return connect(sockfd, addr, addrlen);
}

ssize_t lcore_send(int sockfd, const void *buf, size_t len, int flags)
{
    char *level = getenv("LCORE_LOG_LEVEL");
    if (level != NULL && strcmp(level, "info") == 0) {
        fprintf(stderr, "SEND: %zd bytes\n", len);
    }
    return send(sockfd, buf, len, flags);
}

ssize_t lcore_recv(int sockfd, void *buf, size_t len, int flags)
{
    ssize_t ret;
    char *level = getenv("LCORE_LOG_LEVEL");
    ret = recv(sockfd, buf, len, flags);
    if (level != NULL && strcmp(level, "info") == 0) {
        fprintf(stderr, "RECV: %zd bytes\n", ret);
    }
    return ret;
}

void lcore_info(const char *str)
{
    char *level = getenv("LCORE_LOG_LEVEL");
    if (level != NULL && strcmp(level, "info") == 0) {
        fprintf(stderr, "%s\n", str);
    }
}
